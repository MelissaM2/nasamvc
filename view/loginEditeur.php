<?php $title = 'loginEditeur'; ?>

<?php ob_start(); ?>
<div class="block">

<h1>Bienvenue 
  <?php 
  if(isset($_SESSION['Prénom'])) 
  {
    echo($_SESSION['Prénom']);
    }
  ?>
</h1>
  <br>
    <h2>Les inscriptions</h2>
    <p><mark>ATTENTION : Toute suppression sera définitive.</mark></p>

    <table class="table table-striped" id="table" >
  <thead>
    <tr>
      <td scope="col">Inscrit</td>
      <td scope="col">nom</td>
      <td scope="col">Prénom</td>
      <td scope="col">email</td>
    </tr>
  </thead>
  <tbody>
    
   
      <?php
      $membreManager = new MembresManager();
      $lesMembres = $membreManager->getAllMembres();
        foreach ($lesMembres as $membre) 
        {
          echo("<tr id='info'>");
          echo("<th scope='row' id='btnReplace' class='text-success'>"."Oui"."</th>");
          echo("<td name='nom' class='text-white'>".$membre->getNom()."</td>");
          echo("<td name='prenom' class='text-white'> ".$membre->getPrénom()."</td>");
          echo("<td name='email' class='text-white'> ".$membre->getEmail()."</td>");
          echo("<td><button id='btn' type='button' class='btn btn-outline-danger'><a href='./?action=Editeur&route=routeDeleteMembre&id=".$membre->getId()."'>".'Supprimer'."</a></button></td>");
          echo("</tr>");
       
        }
        
        
        ?>
          
    
  </tbody>
</table>
<br>

</div>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>
