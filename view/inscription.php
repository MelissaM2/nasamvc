<?php $title = 'Inscription'; ?>

<?php ob_start(); ?>

<div class="block">

<?php
if (isset($resultat))
{
if($resultat==true) {
  echo ("<h1 class='text-success'>Inscription réussie</h1>");
}
else{
  echo ("<h1 class='text-danger'>Echec de l'inscription </h1>");
  echo("<p class='text-danger'>$motif</p>");
}
} ?>
            <h1>Formulaire d'Inscription</h1>

            <p>Depuis les années 1960, la NASA a sélectionné 350 personnes pour se former en tant que candidats astronautes pour ses missions de plus en plus difficiles d'exploration de l'espace. Avec 48 astronautes dans le corps d'astronautes actif, il en faudra plus pour équiper des vaisseaux spatiaux à destination de plusieurs destinations et propulser l'exploration vers l'avant dans le cadre des missions Artemis et au-delà.<br><br>

«Nous célébrons cette année notre 20e année de présence continue à bord de la Station spatiale internationale en orbite terrestre basse, et nous sommes sur le point d'envoyer la première femme et le prochain homme sur la Lune d'ici 2024», a déclaré Jim, administrateur de la NASA. Bridenstine. «Pour la poignée de femmes et d'hommes très talentueux que nous embaucherons pour rejoindre notre corps diversifié d'astronautes, c'est une période incroyable dans le vol spatial habité pour devenir astronaute. Nous demandons à tous les Américains éligibles s'ils ont ce qu'il faut pour postuler à partir du 2 mars. <br><br>

Les exigences de base pour postuler comprennent la citoyenneté américaine et une maîtrise dans un domaine STEM, y compris l'ingénierie, les sciences biologiques, les sciences physiques, l'informatique ou les mathématiques, d'une institution accréditée. L'exigence de la maîtrise peut également être remplie par:</p>
<ul>
  <li>Deux ans (36 heures-semestre ou 54 quarts d'heures) de travail en vue d'un doctorat. programme dans un domaine connexe des sciences, de la technologie, de l'ingénierie ou des mathématiques;
</li>
  <li>Un doctorat en médecine ou un doctorat en ostéopathie;
</li>
  <li>Achèvement (ou inscription actuelle qui aboutira à l'achèvement d'ici juin 2021) d'un programme d'école pilote d'essai reconnu à l'échelle nationale.
</li>
</ul>
  <p>Les candidats doivent également avoir au moins deux ans d'expérience professionnelle connexe, à des niveaux de responsabilité de plus en plus élevés, ou au moins 1000 heures de pilote commandant de bord dans un avion à réaction. Les candidats astronautes doivent réussir le vol spatial de longue durée de la NASA.</p>



<div class="inscription">

        <form method="post" action="./?action=routeHandleInscription&amp">

         Nom : <input type="text" name="nom" id="nom" class="saisie">

         Prénom : <input type="text" name="prenom" id="prenom" class="saisie">

         Date de Naissance : <input type="date" id="datepicker" name="dateNaissance" class="saisie">

         Email : <input type="text" name="email" id="email" class="saisie" min="6">

         Mot de Passe : <input type="password" name="mdp1" id="mdp1" class="saisie" minlength="6">

         Verification Mot de Passe : <input type="password" name="mdp2" id="mdp2" class="saisie" minlength="6">

         Adresse : <input type="text" name="adresse" id="adresse" class="saisie">

         Code postal: <input type="text" name="codePostal" id="codePostal" class="saisie">

         Ville : <select name="numVille" id="villes" class="saisie"></select>

         Diplome : <select multiple name="diplome[]" id="diplome" class="saisie"></select>

         Motivation :<br> <textarea name="message" id="message" class="saisie"></textarea>
         <br><br>
         <!-- bouton inscription -->
         <input type="submit" value="S'inscrire" id="valide">
         <div id="divmessage"></div>
       </form>
<br /><br />
</div>
</div>

<script src="public/js/jquery-ui.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="./public/js/inscription/inscription.js"></script>
<script src="js/fonctionsajax.js"></script>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>
