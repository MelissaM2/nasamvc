<?php $title = 'Galerie'; ?>

<?php ob_start(); ?>

   <object class="gallery" data="./view/gallerybis.html" type="text/html"></object>
<div class="d-flex justify-content-center">


   <div class="card border-primary " style="width: 30rem">
     <div class="card-header">
       <h3>Image du Rover sur Mars</h3>
       </div>
       <br><br>
     <div id ="api" class="card-img-top" style="" alt="Card image cap"></div>
     <div class="card-body">
       <p class="card-text text-primary">Cette photo à été prise par <b>Rover</b> sur la planète Mars.</p>
     </div>
   </div>
</div>
<!-- <div class="block">

   <div id ="api" class=""></div>
</div> -->

   <script type="text/javascript" src="./public/js/apiRover.js"> </script>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>
