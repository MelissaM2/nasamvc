<?php

require("./model/bdd.php");

function Accueil()
{
    require('view/Accueil.php');
}

function Galaxie()
{
    require('view/Galaxie.php');
}

function Gallerie()
{
    require('view/Gallerie.php');
}
function Contact()
{
    if(isset($_GET['etat'])&&isset($_GET['msg']))
    {
        $etat=$_GET['etat'];
        $msg=$_GET['msg'];
    }
    require('view/Contact.php');
}
function Inscription()
{
    if(isset($_GET['resultat']))
    {
        $resultat=$_GET['resultat'];
        if(isset($_GET['e']))
        {
            $motif=$_GET['e'];
        }
        //TODO Securité
    }
    require('view/Inscription.php');
}
function Connexion()
{
    require('view/Connexion.php');
}

function login()
{
    require('view/login.php');
}

function  routeJSONVilles(){
    $villeManagers= new VilleManager;
    $JSON=$villeManagers->getVilles();
    require("./view/JSONView.php");
}

function routeJSONDiplomes(){
    $diplomeBDD= new DiplomeManager;
    $JSON=$diplomeBDD->getDiplomes();
    require("./view/JSONView.php");
}
function routeHandleInscription()
{

$nom=test_input($_REQUEST['nom']);
$prenom=test_input($_REQUEST['prenom']);
$dateNaissance=test_input($_REQUEST['dateNaissance']);
$email=test_input($_REQUEST['email']);
$adresse=test_input($_REQUEST['adresse']);
$ville=test_input($_REQUEST['numVille']);
$message=test_input($_REQUEST['message']);
$cp=test_input($_REQUEST['codePostal']);
$diplomes=$_REQUEST['diplome'];
$mdp=test_input($_REQUEST['mdp1']);
$mdp=sha1($mdp);
$verifMdp=test_input(sha1($_REQUEST['mdp2']));
if($mdp==$verifMdp)
{
    if(strlen($mdp>=6))
    {
        if(count($diplomes)>=1)
        {
            $membreManager= new MembresManager();
            $id=$membreManager->addMembre($nom,$prenom,$dateNaissance,$email,$adresse,$message,$ville,$cp,$mdp);
            foreach($diplomes as $numDiplome)
            {
                $membreManager->addDiplomesForMembre($id,$numDiplome);
                header("Location: http://localhost/nasamvc/index.php?action=Inscription&resultat=1");
            }
            
        }
        else{
            header("Location: http://localhost/nasamvc/index.php?action=Inscription&resultat=0&e=Pas%20%de%20Diplomes");
        }
    }
    else {
        header("Location: http://localhost/nasamvc/index.php?action=Inscription&resultat=0&e=MDP%20%est%20trop%20court");
    }
}
else{
    header("Location: http://localhost/nasamvc/index.php?action=Inscription&resultat=0&e=MDP%20%ne%20concordent%20pas");
}



}
function routeLogin()
{
    
    if(isset($_SESSION['id']))
    {
        $_SESSION = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
            header("location: http://localhost/nasamvc/index.php?action=Connexion");
        }
    }
    require("view/Connexion.php");
}

function  handleLogin($email,$mdp){
    $membreManager= new MembresManager();
    $resultat=$membreManager->verificationLogin($email,$mdp);
    if($resultat==false)
    {
        echo("l'email ou le mot de passe est incorrect");
        Connexion();
        die;
    }
    else{
        $_SESSION['role']=$resultat->getRole();
        $_SESSION['email']=$resultat->getEmail();
        $_SESSION['Nom']=$resultat->getNom();
        $_SESSION['Prénom']=$resultat->getPrénom();
        $_SESSION['id']=$resultat->getId();
        
        if($_SESSION['role']=='Editeur')
        {
            require("./view/loginEditeur.php");
        }
        else
        {
            require('view/login.php');
        }
        
    }

}

function routeVerifMail()
{
    $email = $_REQUEST['email'];
    $membreManager=new MembresManager();
    echo $membreManager->existeMail($email);
}


?>