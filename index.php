<?php

require('controller/controller.php');

session_start();
if( isset($_SESSION['token'])==false)
{
    $_SESSION['token'] = bin2hex(random_bytes(32)); //Token CSRF
}
if(isset($_SESSION['role']))
{
    $role=$_SESSION['role'];
}

try{
    if (isset($_GET['action'])) {
      $action=test_input($_GET['action']);// "teste_input" sécurité pour voir s'il n'y a pas de JS, html ect
        if ($action == 'Accueil') {
          Accueil();
        }
          else if ($action == 'Galaxie') {
            Galaxie();
          }
          else if ($action== 'Gallerie') {
            Gallerie();
          }
          else if ($action== 'Contact') {
            Contact();
          }
          else if ($action == 'Inscription') {
            Inscription();
          }
          elseif ($action=='JSONVilles')
          {
            routeJSONVilles();
          }
          elseif ($action=='JSONDiplomes')
          {
            routeJSONDiplomes();
          }
          elseif($action=="verifMail")
          {
            routeVerifMail();
          }
          elseif ($action=="routeHandleInscription")
          {
            if(isset($_POST['nom'],$_POST['prenom'],$_POST['dateNaissance'],$_POST['email'],$_POST['mdp1'],$_POST['mdp2'],$_POST['adresse'],$_POST['codePostal'],$_POST['numVille'],$_POST['diplome'],$_POST['message'])){
              routeHandleInscription();
            }
            else {
              header("Location: http://localhost/nasamvc/index.php?action=Inscription&resultat=0&e=Formulaire%20invalide");
            }
          }

          else if($action=="Connexion")
          {
            routeLogin();
          }

          else if($action=="handleLogin"&&isset($_POST['email'],$_POST['mdp'],$_POST['CSRF']))
          {
            $email=test_input($_POST['email']);
            $mdp=test_input($_POST['mdp']);
            $mdp=sha1($mdp);
            $CSRF=test_input($_POST['CSRF']);// sécurité pour la confirmation de l'envoie d'infos grâce au token qui va envoyer un <input type="hidden" value="(le numero aleatoire)"

            if($CSRF!=$_SESSION['token'])// la sécurité va se mettre en place ici
            {
              login();
            }
            else{
              handleLogin($email,$mdp);
            }
          }

          //ROUTE EDITEUR
          elseif($action=="Editeur")
          {
            if(isset($_SESSION['role']))
            {
              if($_SESSION['role']=="Editeur")
              {
                $route=test_input($_GET['route']);
                if($route=="routeDeleteMembre")
                {
                  require("controller/editeurController.php");
                  $id=test_input($_GET['id']);
                  routeDeleteMembre($id);
                }
                require("controller/editeurController.php");
                routePageEditeur();
              }
              else{
                Accueil();
              }
            }
          }
          //FIN ROUTE EDITEUR

          //ROUTES utilisateur
          elseif($action=="membre")
          {
            if(isset($_SESSION['role']))
            {
              if($_SESSION['role']=="membre")
              {
                require("controller/membreController.php");
                routePageMembre();
              }
              else{
                Accueil();
              }
            }
          }
          //FIN ROUTE ADMIN
        else{
            throw new Exception ("Pas de page pour cette action");
        }
    }
    else {
        Accueil();
    }


  }
catch(Exception $e){
    echo 'Erreur : ' . $e->getMessage();
}

//sécurité
function test_input($data){
  $data=urldecode($data);
  $data=trim($data);//enlever les espace
  $data=stripslashes($data);// enlever les slash
  $data=htmlspecialchars($data);// parser le code html
  return $data;
}

/* index.php Est le routeur il va :
*   - Regarder quelle est la valeur du param action
*   - Generalement il vas allez chercher des données dans le model
*   - Et utiliser invoquer (appeler) une fonction du controlleur en lui passant des données (du model) si besoin
*/
?>
