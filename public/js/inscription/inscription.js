$("#valide").click
(
     function(event)
     {

            var email=$("#email").val();
            var nom=$("#nom").val();
            var prenom=$("#prenom").val();
            var adresse=$("#adresse").val();
            var date=$("#datepicker").val();
            var cp=$("#codePostal").val();
            var message=$("#message").val();
            var mdp1=$("#mdp1").val();
            var mdp2=$("#mdp2").val();

            var formValide=true;

        if(estRempli("saisie","red"))
            {

               if(!concordance(mdp1,mdp2))
               {
                    // on colore en orange les deux zones
                    $("#mdp1").css("background","orange");
                    $("#mdp2").css("background","orange");
                    formValide=false;
                    event.preventDefault();
               }
               else if(!mdpvalide(mdp1))
               {
                   $("#mdp1").css("background","orange");
                   formValide=false;
                   event.preventDefault();
               }

               if(existemail("./?action=verifMail",email)=="1")
               {

                    $("#divmessage").css("color","red");
                    $("#divmessage").html("Adresse existante");
                    formValide=false;
                    event.preventDefault();
                    // return false;
               }

               if(formValide)
               {
                  $("#divmessage").css('color','forestgreen');
                  $("#divmessage").html("Inscription réussie");
               }

            }
      else {
        event.preventDefault();
        // return false;
      }
     }

);

$(".saisie").click
(
     function()
     {

        $(".saisie").css("background","white");
     }
);


function estRempli(classe,couleur)
{
	resultat=true;
	 $(".saisie").each
	 (
	       function()
		   {
			   var valeur=$(this).val();
			   if(!valeur)
			   {
				  resultat=false;
				  $(this).css({'background':couleur});
			   }
		   }
	 );
	 return resultat;
}

/* procédure qui rétablit l'arrière-plan d'origine sur les zones concernées*/

function saisie(classe,macouleur)
{
	$('.form-control').each
	(
	function()
		{
			$(this).css('background','white');

		}
	);
}


function mdpvalide(mdp)
{
		return mdp.length>=6;
}

function concordance(chaine1,chaine2)
{
	return chaine1==chaine2;
}


function existemail(urlphp,email)
{
    $.ajax
	   (
	      {
			  url:urlphp,
				data:{"email":email},
				async:false,
				complete:function(resultat)
				{
				      res= resultat.responseText;

				}
			}

	   );
	   return res;
}

// timothe
async function fetchJSON(url)
{
  let reponse= await fetch(url);
  let data=await reponse.json();
  return data;
}

/**
 * Evenement qui se déclenche quand on quite l'input email
 */
$("#email").blur(
 //Le code qui se déclenche prend la forme d'une fonction anonyme auto-invoquer
  function()
  {
    //Si le mail existe deja dans la BDD
    if(existemail("./?action=verifMail",$("#email").val())==1)
    {
      //Alors on change la couleur de l'input
      $("#email").css('background',"orange")
      //Et on affiche une alert
      alert("L'adresse mail existe déja");
    }
   
  }
 
);

fetchJSON("http://localhost/nasaMVC?action=JSONVilles").then(function(result){
  let select=document.getElementById("villes");
  let chaineHTML="";
  for(let ville of result){
    chaineHTML+='<option value='+ville[0]+'> '+ville[1]+'</option>';
  }
  select.innerHTML=chaineHTML;
})

fetchJSON("http://localhost/nasaMVC?action=JSONDiplomes").then(function(result){
  let select=document.getElementById("diplome");
  let chaineHTML="";
  for(let diplome of result){
    chaineHTML+='<option value='+diplome[0]+'> '+diplome[1]+'</option>';
  }
  select.innerHTML=chaineHTML;
})
