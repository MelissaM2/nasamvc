<?php

class MembresManager extends BDD
{
    function __construct()
    {
        parent::__construct();
    }
    function existeMail($email)
    {
      
    $sql=$this->connex->prepare("SELECT existemail(:em) as res");
    $sql->bindValue(":em",$email);
    $sql->execute();
    $resultat=$sql->fetch();
    $rep="0"; // valeur par defaut correspond à aucun resultat trouve
    // on definit un curseur d'enregistrements
    $sql->setFetchMode(PDO::FETCH_OBJ);
    $rep=$resultat['res'];
    $sql->closeCursor();
    return $rep;
        
      
    }

    function getMembresBy($nomCol,$valCol)
    {
        try{

            $req="SELECT * FROM membres WHERE $nomCol = :valCol ";
            $stm=$this->connex->prepare($req);
            $stm->setFetchMode(PDO::FETCH_CLASS, 'Membre');
            $stm->bindParam(":valCol",$valCol); // sécurité contre les injections SQL
            $stm->execute();
            $result = $stm->fetchAll();
            return $result;
            }
            catch(PDOException $erreur)
            {
              echo $erreur->getMessage();
            }
    }
     function addMembre($nom,$prenom,$date,$email,$adresse,$message,$ville,$cp,$mdp)
     {
        $req="INSERT INTO membres VALUES (null,:nom,:prenom,:dateNaissance,:email,:adresse,:message,:ville,:cp,:mdp,'membre')";


        $resultat=$this->connex->prepare($req);

        $resultat->bindValue(':nom',$nom);
        $resultat->bindValue(':prenom',$prenom);
        $resultat->bindValue(':dateNaissance',$date);
        $resultat->bindValue(':email',$email);
        $resultat->bindValue(':adresse',$adresse);
        $resultat->bindValue(':message',$message);
        $resultat->bindValue(':ville',$ville);
        $resultat->bindValue(':cp',$cp);
        $resultat->bindValue(':mdp',$mdp);
        //$connex->exec($req);

        $resultat->execute();

        $id=$this->connex->lastInsertId();
        return $id;
     }
     function addDiplomesForMembre($idMembre,$idDiplome)
     {
         
       try{
       $req="INSERT INTO passer VALUES (:idMembre,:idDiplome, null)";

       $stm=$this->connex->prepare($req);

       $stm->bindValue(':idMembre',$idMembre);
       $stm->bindValue(':idDiplome',$idDiplome);
       $stm->execute();
       }
       catch(PDOException $erreur)
       {
         echo $erreur->getMessage();
       }
     }
     function verificationLogin($email,$mdp)
    {
        try {
            $sql = $this->connex->prepare("SELECT * FROM membres where email=:email and Mdp=:mdp"); //":email" est une clé
            $sql->setFetchMode(PDO::FETCH_CLASS, 'Membre');
            $sql->bindParam(":email", $email);
            $sql->bindParam(":mdp", $mdp);
            $sql->execute();
            $resultat = ($sql->fetchAll());
            if(count($resultat)>=1)
            {
                return $resultat[0];
            }
            else{
                return false;
            }
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    function getAllMembres(){
        try{
    
            $req="select * from membres";
            $stm=$this->connex->prepare($req);
            $stm->setFetchMode(PDO::FETCH_CLASS, 'Membre');
            $stm->execute();
            $result = $stm->fetchAll();
    
            return $result;
    
            }
            catch(PDOException $erreur)
            {
              echo $erreur->getMessage();
            }
        }

    function deleteMembre($id)
    {
        try{
        $reque1="DELETE from passer where id=:idMembre";

        $re1=$this->connex->prepare($reque1);
        $re1->bindParam(":idMembre",$id);
        $re1->execute();
        $reque2="DELETE from membres WHERE id = :idMembre";
        $re=$this->connex->prepare($reque2);
        $re->setFetchMode(PDO::FETCH_CLASS, 'Membre');
        $re->bindParam(":idMembre",$id);

        $re->execute();
        

        }

        catch(PDOException $erreur)
            {
              echo $erreur->getMessage();
            }
    }
}
?>
