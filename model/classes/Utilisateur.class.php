<?php
class Utilisateur {
    private $NumUtilisateur;
    private $PrenomUtilisateur;
    private $NomUtilisateur;
    private $Email;
    private $Mdp;
    private $Role;

    function getNumUtilisateur()
    {
        return $this->NumUtilisateur;
    }
    function setNumUtilisateur($Num)
    {
        $this->NumUtilisateur=$Num;
    }

    function getPrenomUtilisateur()
    {
        return $this->PrenomUtilisateur;
    }
    function setPrenomUtilisateur($Prenom)
    {
        $this->PrenomUtilisateur=$Prenom;
    }

    function getNomUtilisateur()
    {
        return $this->NomUtilisateur;
    }
    function setNomUtilisateur($Nom)
    {
        $this->NomUtilisateur=$Nom;
    }

    function getEmail()
    {
        return $this->Email;
    }
    function setEmail($email)
    {
        $this->Email=$email;
    }

    function getMdp()
    {
        return $this->Mdp;
    }
    function setMdp($mdp)
    {
        $this->Mdp=$mdp;
    }

    function getRole()
    {
        return $this->Role;
    }
    function setRole($role)
    {
        $this->Role=$role;
    }
}

?>
